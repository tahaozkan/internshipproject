const faker = require('faker');
const Promise = require('bluebird');
const fs = require('fs');
const opSelector = require('./opSelector.js');
var uppercaseKeys = require('uppercase-keys'); // for converting uppercase

faker.locale = 'tr'; // Settings for faker

const graphson = {
  graph: {
    vertices: [],
    edges: [],
  },
};



//id's are unique, and the following counters are to be used to generate id's

let customerCount = 0;        //counts for customer id
let eftCount = 0;             //counts for eft id
let HavaleCount = 0;          //counts for havale id
let CEKCount = 0;
let senetCount = 0;
let sequentialIndex = 0;
let generatedNumbers = [];      //to be used to check if the generated thing; ie, MUSNO is unique

function generateNumberByDigit(digit){                 //generates a num with given digit number
  let num = 9;
  let num2 = 1;
  for(let i = 0; i < digit-1; i += 1)  num *= 10;
  for(let i = 0; i < digit-1; i += 1)  num2 *= 10;
  num = Math.floor(Math.random() * num) + num2;
  return num;
}

//Generates unique numbers with etiher given number of digits or with number of digits in a given interval, which is an array
function generateUnique(digit){
  if(digit == undefined){             //no parameter given, ie, can be generated thing can be in any length
    let uniqueNum = generateNumberInInterval(1, 10);
    if( generatedNumbers.includes(uniqueNum) ){       //if generated num already exists
      while(true){
        uniqueNum += 1;
        if( !(generatedNumbers.includes(uniqueNum)) ) break;
        else continue;
      }
      generatedNumbers.push(uniqueNum);
      return uniqueNum;
    }
    else {return uniqueNum; }
  }
  //unique number to be generated has n-digits
  let uniqueNum = generateNumberByDigit(digit);         //digit basamaklı sayı üretildi
  if(generatedNumbers.includes(uniqueNum)){      //make sure it is unique
    while(true){
      uniqueNum += 1;
      if( !(generatedNumbers.includes(uniqueNum)) ) break;
      else continue;
    }
  }
  generatedNumbers.push(uniqueNum);
  return uniqueNum;

}

function generateMartialStatus() {
  const status = ['Evli', 'Bekar', 'Dul'];
  return status[Math.floor(Math.random() * status.length)];
}

function generateEducationStatus() {
  const status = ['ILKOGRETİM TERK', 'ORTAOGRETIM TERK', 'UNIVERSITE TERK',
    'ILKOĞRETIM MEZUNU', 'ORTAOĞRETIM MEZUNU', 'UNIVERSITE MEZUNU', 'UNIVERSITE OGRENCISI'];
  return status[Math.floor(Math.random() * status.length)];
}

function generateGender() {
  const gender = ['ERKEK', 'KADIN', 'BELIRTIRMEMIS'];
  return gender[Math.floor(Math.random() * gender.length)];
}

function bankNameGenerator() {
  const banks = ['Türkiye Cumhuriyeti Ziraat Bankası A.Ş.', 'Türkiye Halk Bankası A.Ş.',
    'Türkiye Vakıflar Bankası T.A.O.', 'Adabank A.Ş.',
    'Akbank T.A.Ş.', 'Anadolubank A.Ş.', 'Fibabanka A.Ş.',
    'Şekerbank T.A.Ş.', 'Turkish Bank A.Ş.', 'Türk Ekonomi Bankası A.Ş.',
    'Yapı ve Kredi Bankası A.Ş.', 'Birleşik Fon Bankası A.Ş.', 'Alternatifbank A.Ş.',
    'Bank of China Turkey A.Ş.', 'Burgan Bank A.Ş.', 'Citibank A.Ş.', 'Denizbank A.Ş.',
    'Deutsche Bank A.Ş.', 'HSBC Bank A.Ş.', 'ICBC Turkey Bank A.Ş.', 'ING Bank A.Ş.',
    'MUFG Bank Turkey A.Ş.', 'Odea Bank A.Ş.', 'QNB Finansbank A.Ş.', 'Rabobank A.Ş.',
    'Turkland Bank A.Ş.', 'Türkiye Garanti Bankası A.Ş.'];
  return banks[Math.floor(Math.random() * banks.length)];
}

async function generateMultiplePersonalCustomers(desiredPersonalCustomerCount){
  try{
    for (let i = 0; i < desiredPersonalCustomerCount; i += 1) {
      generatePersonalCustomer();
    }
  } catch (error) {
    return Promise.reject(new Error(`Error at generating multiple personal customers: ${error}`));
  }
}

async function generateMultipleCommercialCustomers(desiredCommercialCustomerCount) {
  try {
    for (let i = 0; i < desiredCommercialCustomerCount; i += 1) {
      generateCommercialCustomer();
    }
  } catch (error) {
    return Promise.reject(new Error(`Error at generating multiple commercial customers: ${error}`));
  }
}

async function generateMultipleOtherBankCustomers(otherBankCount) {
  try {
    for (let i = 0; i < otherBankCount; i += 1) {
      generateOtherBankCustomer();
    }
  } catch (error) {
    return Promise.reject(new Error(`Error at generating multiple foreign bank customers:`));
  }
}

function generatePersonalCustomer() {
  try {
    newPersonalCustomer = generateNewNode('personal');
    customerCount += 1;
    graphson.graph.vertices.push(newPersonalCustomer);
    return Promise.resolve(true);
  } catch (error) {
    return Promise.reject(new Error(`Error at generating personal customer: ${error}`));
  }
}

function generateCommercialCustomer() {
  try {
    newCommercialCustomer = generateNewNode('commercial');
    customerCount += 1;
    graphson.graph.vertices.push(newCommercialCustomer);
  } catch (error) {
    return Promise.reject(new Error(`Error at generating Commercial customer: ${error}`));
  }
}

function generateOtherBankCustomer() {
  try {
    newOtherBankCustomer = generateNewNode('KB_MUSTERI');
    customerCount += 1;
    graphson.graph.vertices.push(newOtherBankCustomer);
  } catch (error) {
    return Promise.reject(new Error(`Error at generating other bank customer: ${error}`));
  }
}

//to be altered to deal with different types of nodes
function generateNewNode(type){
  const year = Math.floor(Math.random() * 28) + 1990;
  const month = Math.floor(Math.random() * 12)+1;


  let dogumTarihi = (new Date(year, month)); //
  let yenitarih= new Date(dogumTarihi.toString()); //
  let formatted_date = yenitarih.getUTCFullYear() + "-" + (yenitarih.getUTCMonth() + 1) +"-" + yenitarih.getUTCDate()+" 0"+ Math.floor(Math.random()*12)+":"+ yenitarih.getUTCHours()+"+0"+(yenitarih.getTimezoneOffset()/-60)+":00"; // set gmt date format


  if(type === 'personal'){            //personal customer generator
    gender = generateGender();
    const newNode = {
      MUS_NO: generateUnique(9),
      DOGUM_TRH: formatted_date,
      SOYAD: faker.name.lastName(gender),
      MEDENI_DURUM: generateMartialStatus(),
      TCKN: Math.floor(Math.random() * 900000000000) + 100000000000,
      AD: faker.name.firstName(gender),
      OGRENIM_DURUM: generateEducationStatus(),
      MUS_SAHIP_SBKD: Math.floor(Math.random() * 9000) + 1000,
      MUS_KILAVUZ_SBKD: Math.floor(Math.random() * 9000) + 1000,
      YERLESIK_IL_KOD: Math.floor(Math.random() * 81) + 1,
      CINSIYET: gender,
      label: 'BIREYSEL_MUSTERI',
      _id: customerCount,
      _type: 'vertex',
    };
    return newNode;
  }
  else if(type === 'commercial'){      //commercial customer generator
    const newNode = {
      MUS_NO: generateUnique(9),
      VKN: Math.floor(Math.random() * 9000000000) + 1000000000,
      UNVAN: faker.name.jobTitle(),
      MUS_SAHIP_SBKD: Math.floor(Math.random() * 9000) + 1000,
      MUS_KILAVUZ_SBKD: Math.floor(Math.random() * 9000) + 1000,
      YERLESIK_IL_KOD: Math.floor(Math.random() * 81) + 1,
      label: 'KURUMSAL_MUSTERI',
      _id: customerCount,
      _type: 'vertex',
    };
    return newNode;
  }
  else if(type === 'KB_MUSTERI'){                //otherBank customer generator
    const gender = generateGender();
    const newNode = {
      AD: faker.name.firstName(gender),
      SOYAD: faker.name.lastName(gender),
      KB_MUSTERI_NUMARASI: generateUnique(9),
      KATILIMCI_ADI: bankNameGenerator(),
      label: 'KB_MUSTERI',
      _id: customerCount,
      _type: 'vertex',
    };
    return newNode;
  }
}

async function generateMultipleHavale(desiredHavaleCount){
  try {
    let counter = 0;
    for(let i = 0; ; i+=1){
      let index1 = Math.floor(Math.random() * graphson.graph.vertices.length);
      let person1 = graphson.graph.vertices[index1];
      let index2 = Math.floor(Math.random() * graphson.graph.vertices.length);
      let person2 = graphson.graph.vertices[index2];
      if(person1.label === 'KB_MUSTERI' || person2.label === 'KB_MUSTERI' || person1._id == person2._id) continue;
      else{                                                  //person1 and person2 are appropriate for havale
        generateHavale(person1, person2);       //generates havale from person1 to person2
        counter += 1;                                      //increment if it can be generated, maybe with try{}.catch()

        if(counter == desiredHavaleCount) return;        //desired count is reached
      }

    }
  } catch (error) {
    return Promise.reject(new Error(`Error at saving multiple EFT's: ${error}`));
  }
}

async function generateMultipleEFTs(desiredEFTCount) {
  try {
    let counter = 0;
    for(let i = 0; ; i+=1){
      let index1 = Math.floor(Math.random() * graphson.graph.vertices.length);
      let person1 = graphson.graph.vertices[index1];
      let index2 = Math.floor(Math.random() * graphson.graph.vertices.length);
      let person2 = graphson.graph.vertices[index2];
      if( (person1.label === 'KB_MUSTERI' && person2.label === 'KB_MUSTERI') || (person1.label === 'BIREYSEL_MUSTERI' && person2.label === 'BIREYSEL_MUSTERI') || (person1.label === 'KURUMSAL_MUSTERI' && person2.label === 'KURUMSAL_MUSTERI') ||  (person1.label === 'KURUMSAL_MUSTERI' && person2.label === 'BIREYSEL_MUSTERI')  ||  (person1.label === 'BIREYSEL_MUSTERI' && person2.label === 'KURUMSAL_MUSTERI') ||person1._id == person2._id  ) continue;      //Havale cannot be b/w kbmusteris and same person(s)
      else{                                                  //person1 and person2 are appropriate for EFT
        generateEFT(person1, person2);                  //generates havale from person1 to person2
        counter += 1;                                      //increment if it can be generated, maybe with try{}.catch()
        if(counter == desiredEFTCount) return;        //desired count is reached
      }
    }
  } catch (error) {
    return Promise.reject(new Error(`Error at saving multiple EFT's: ${error}`));
  }
}

//generate havale from person1 to person2 who are guaranteed not to be KB_MUSTERİ and same persons, as controlled in generateMultipleHavale
async function generateHavale(person1, person2){
  try {
    const senderID = person1._id;
    const receiverID = person2._id;
    const havaleObj = opSelector.selectHavale();               //havaleObj's id may be 'local' or 'foreign'
    edgeHavaleGenerator(senderID, receiverID, havaleObj);
    HavaleCount += 1;                                    //new havale to be generated
    sequentialIndex +=1;
  } catch (error) {
    return Promise.reject(new Error(`Error at saving EFT: ${error}`));
  }
}


async function generateEFT(person1, person2) {
  try {
    const senderID = person1._id;
    const receiverID = person2._id;
    const EFTObj = opSelector.selectEFT();
    edgeEFTGenerator(senderID, receiverID, EFTObj);
    eftCount += 1;
    sequentialIndex +=1;
  } catch (error) {
    return Promise.reject(new Error(`Error at saving EFT: ${error}`));
  }
}

function edgeHavaleGenerator(senderID, receiverID, havaleObj){      //to be altered to deal with all types of edges
  try{
    let newEdge = {
      HAVALE_DOVIZ_TUTARI: havaleObj.amount,
      MASRAF_DOVIZ_TUTARI: havaleObj.amount,
      HAVALE_TIPI: havaleObj.type,
      HAVALE_TL_TUTARI: 0,
      MASRAF_TL_TUTARI: havaleObj.cost,
      KOMISYON_DOVIZ_TUTARI: 0,
      TARIH: (String)(havaleObj.date),
      KOMISYON_TL_TUTARI: havaleObj.commission,
      HAVALE_DOVIZ_KODU: havaleObj.currCode,
      _id: sequentialIndex,
      _type: 'edge',
      _outV: senderID,                //_outV = senderID
      _inV: receiverID,               //_inV = receiverID
      _label: 'HAVALE',
    };
    if(havaleObj.id == 'local'){
      newEdge.HAVALE_DOVIZ_TUTARI = 0;
      newEdge.HAVALE_TL_TUTARI = havaleObj.amount;
      newEdge.HAVALE_DOVIZ_KODU = '';
    }
    graphson.graph.edges.push(newEdge);

  } catch (error) {
    return Promise.reject(new Error(`Error at generating Havale(Havale): ${error}`));
  }
}

function edgeEFTGenerator(senderID, receiverID, EFTObj){          //to be altered to deal with all types of edges
  try{
    let newEdge = {
      MASRAF_TUTAR: EFTObj.cost,
      TARIH: (String)(EFTObj.date),
      TUTAR: EFTObj.amount,
      _id: sequentialIndex,
      _type: 'edge',
      _outV: senderID,                          //outV = senderI
      _inV: receiverID,                         //inV = receiverID
      _label: 'EFT',
    };

    graphson.graph.edges.push(newEdge);
  } catch (error) {
    return Promise.reject(new Error(`Error at generating Havale(Havale): ${error}`));
  }
}

async function generateMultipleCEK(desiredCekCount){
  try {
    let counter = 0;
    for(let i = 0; ; i+=1){
      let index1 = Math.floor(Math.random() * graphson.graph.vertices.length);
      let person1 = graphson.graph.vertices[index1];
      let index2 = Math.floor(Math.random() * graphson.graph.vertices.length);
      let person2 = graphson.graph.vertices[index2];
      if( (person1.label === 'KB_MUSTERI' && person2.label === 'KB_MUSTERI') || person1._id == person2._id ) continue;
      generateCEK(person1, person2);
      counter += 1;
      sequentialIndex +=1;
      if(counter == desiredCekCount) return;
    }
  } catch (error) {
    return Promise.reject(new Error(`Error at saving multiple EFT's: ${error}`));
  }
}

async function generateCEK(person1, person2){
  try {
    const senderID = person1._id;
    const receiverID = person2._id;
    edgeCEKGenerator(senderID, receiverID);
    CEKCount += 1;                                    //new havale to be generated
  } catch (error) {
    return Promise.reject(new Error(`Error at saving EFT: ${error}`));
  }
}

function edgeCEKGenerator(senderID, receiverID){      //to be altered to deal with all types of edges
  try{
    let newEdge = {
      CEK_TL_TUTARI: generateNumberByDigit(5),
      KOMISYON_DOVIZ_TUTARI: 0,
      _id: sequentialIndex,
      _type: 'edge',
      _outV: senderID,                //_outV = senderID
      _inV: receiverID,               //_inV = receiverID
      _label: 'ÇEK',
    };

    graphson.graph.edges.push(newEdge);

  } catch (error) {
    return Promise.reject(new Error(`Error at generating Havale(Havale): ${error}`));
  }
}

async function generateMultipleSenets(desiredSenetCount){
  try {
    let counter = 0;
    for(let i = 0; ; i+=1){
      let index1 = Math.floor(Math.random() * graphson.graph.vertices.length);
      let person1 = graphson.graph.vertices[index1];
      let index2 = Math.floor(Math.random() * graphson.graph.vertices.length);
      let person2 = graphson.graph.vertices[index2];
      if( (person1.label === 'KB_MUSTERI' && person2.label === 'KB_MUSTERI') || person1._id == person2._id ) continue;
      generateSenet(person1, person2);
      counter += 1;
      sequentialIndex +=1;
      if(counter == desiredSenetCount) return;
    }
  } catch (error) {
    return Promise.reject(new Error(`Error at saving multiple EFT's: ${error}`));
  }
}

async function generateSenet(person1, person2){
  try {
    const senderID = person1._id;
    const receiverID = person2._id;
    edgeSenetGenerator(senderID, receiverID);
    senetCount += 1;                                    //new havale to be generated
  } catch (error) {
    return Promise.reject(new Error(`Error at saving EFT: ${error}`));
  }
}

function edgeSenetGenerator(senderID, receiverID){      //to be altered to deal with all types of edges
  try{
    let newEdge = {
      SENET_TL_TUTARI: generateNumberByDigit(5),
      KOMISYON_DOVIZ_TUTARI: 0,
      _id: sequentialIndex,
      _type: 'edge',
      _outV: senderID,                //_outV = senderID
      _inV: receiverID,               //_inV = receiverID
      _label: 'SENET',
    };

    graphson.graph.edges.push(newEdge);

  } catch (error) {
    return Promise.reject(new Error(`Error at generating Havale(Havale): ${error}`));
  }
}

async function generateAllLinks(desiredHavaleCount, desiredEFTCount, desiredCekCount, desiredSenetCount){
  try{
    generateMultipleHavale(desiredHavaleCount);
    generateMultipleEFTs(desiredEFTCount);
    generateMultipleCEK(desiredCekCount);
    generateMultipleSenets(desiredSenetCount);
  } catch(error){
    return Promise.reject(new Error('edges of the graph could NOT have been generated'));
  }
}

async function generateAllCustomers(desiredPersonalCustomerCount, desiredCommercialCustomerCount, desiredOtherBankCustomerCount) {
  try {
    generateMultiplePersonalCustomers(desiredPersonalCustomerCount);
    generateMultipleCommercialCustomers(desiredCommercialCustomerCount);
    generateMultipleOtherBankCustomers(desiredOtherBankCustomerCount);
  } catch (error) {
    return Promise.reject(new Error(`Error at creating customers: ${error}`));
  }
}

async function mainTrial(){

  let desiredPersonalCustomerCount = 1500;
  let desiredCommercialCustomerCount = 1000;
  let desiredOtherBankCustomerCount = 500;
  let desiredHavaleCount =2000;
  let desiredEFTCount = 1500;
  let desiredCekCount = 1000;
  let desiredSenetCount = 500;

  generateAllCustomers(desiredPersonalCustomerCount, desiredCommercialCustomerCount, desiredOtherBankCustomerCount);
  generateAllLinks(desiredHavaleCount, desiredEFTCount, desiredCekCount, desiredSenetCount);


  //to be used to deal with real big data
  let totalCustomerCount = desiredPersonalCustomerCount + desiredCommercialCustomerCount + desiredOtherBankCustomerCount;
  let totalEdgeCount =  desiredHavaleCount + desiredEFTCount;



  try{
    const str = JSON.stringify(graphson);

    await fs.writeFile('datanew.json', str, function(err, result){
      if(err) throw new Error(`Something went wrong: ${err}`);
      else console.log("DONE!");
    });
    return Promise.resolve(true);
  } catch (error) {
    return Promise.reject(new Error(`Error at driver function: ${error}`));
  }
}


//console.time("500 node, 940 edge");
console.time( mainTrial() );
//console.timeEnd("500 node, 940 edge");


function convertToPgx(){
  fs.readFile('datanew.json', function (err, data) { //reading  json file
    if (err) throw err;
    var mydata = JSON.parse(data); //parsing data
    let verticalStr="";  // for store vertices data
    let edgesArray=[];   // for store edges data


    function convertVertices(){   //convert function for vertices

      mydata.graph.vertices.forEach(obj => {   // foreach block for accessing json data
        obj=uppercaseKeys(obj); // to convert to uppercase all keys

        Object.entries(obj).map(([key, value]) => {

          if(key!="_ID" && key!="_TYPE" )  //for displaying values without id and type and (dogum_tarihi && `${key}`!="DOGUM_TRH")
          {
            if(typeof  value =='number')
            {
              let str = "\n"+obj._ID+","+key+",7,,"+value+",";
              //console.log("number : " + str);
              verticalStr += str
            }
            else if(Date.parse(value) >0 )
            {
              let str = "\n"+obj._ID+","+key+",5,,,"+value;
              //console.log("date : " + str);
              verticalStr += str;
            }
            else if(typeof value=="string")   // if value of key string
            {
              let str  ="\n"+obj._ID+","+key+",1,"+value+",,";
              //console.log("str : " + str);
              verticalStr += str;   //adding values to array
            }

          }

          // number 7 string 1  (date 5)
        });
      });

    }
    convertVertices();
    verticalStr = verticalStr.substr(1);
    //verticesArray[0] = verticesArray[0].replace(/(\r\n|\n|\r)/gm,""); // to remove first blank line \n
    fs.writeFile('dataVertices.opv', verticalStr, function(err, result){ // writing to file
      if(err) throw new Error(`Something went wrong: ${err}`);
      else console.log("OK!");
    });

    function convertEdges(){  //convert function for edges


      mydata.graph.edges.forEach(obj => {
        Object.entries(obj).map(([key, value]) => {
          if(typeof value=="number"){
            if(`${key}`!="_id" && `${key}`!="_type" && `${key}`!="_outV" && `${key}`!="_inV"  && `${key}`!="_label" ){  //for displaying values without id, type,_inV,_outV and label

              edgesArray.push("\n"+obj._id+","+obj._outV+","+obj._inV+","+obj._label+","+key+",7,,"+value);   //adding values to edgesArray
            }

          }


        });
        //console.log('-------------------');

      });
    }
    convertEdges();
    edgesArray.push("");//for adding , to last element of array
    edgesArray[0] = edgesArray[0].replace(/(\r\n|\n|\r)/gm,""); // to remove first blank line

    fs.writeFile('dataEdges.ope', edgesArray, function(err, result){
      if(err) throw new Error(`Something went wrong: ${err}`);
      else console.log("OK!");
    });
  })



}
convertToPgx();


