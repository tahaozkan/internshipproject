const faker = require('faker');
const fs = require('fs')

function generateUsers() {

  let users = []

  for (let id=1; id <= 100; id++) {

    let firstName = faker.name.firstName();
    let lastName = faker.name.lastName();
    let email = faker.internet.email();
    let address=faker.address.city();
    let country=faker.address.country();
    users.push({
        "id": id,
        "first_name": firstName,
        "last_name": lastName,
        "address":address,
        "email": email,
        "country":country
    });
  }

  return { "data": users }
}

let dataObj = generateUsers();
console.log(faker.name == undefined);
fs.writeFileSync('data1.json', JSON.stringify(dataObj, null, '\t'));