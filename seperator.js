const fs = require('fs');
//const newFormat = require('./newFormat.js');

let rawdata = fs.readFileSync('this.json');
let str = JSON.parse(rawdata);

const newGraph = {
    graph: {
      vertices: [],
      edges: [], 
    },
  };

//read from the first generated data
let nodes = str.graph.vertices;
let links = str.graph.edges;

// given nodeID, returns the node with that ID. Retruns -1 if node is not found
function findNode(nodeID){
    try{
        let len = nodes.length;
        for(let i = 0; i<len; i += 1){
            if(nodes[i]._id == nodeID) return nodes[i];
            else continue;
        }
        return -1;
    }catch(error){
        Promise.reject(new Error(`Error at findNode: ${error}`));
    }
}

//returns if the given link already exists in the given array: edges
function doesLinkExist(link, edges){
    try{
        let len = edges.length;
        for(let i = 0; i<len; i += 1){
            if(edges[i]._id == link._id && edges[i]._label == link._label) return true;
            else continue;
        }
        return false;
    }
    catch(error){
        Promise.reject(new Error(`Error at doesLinkExist: ${error}`));
    }
}

//returns if the given node already exists in the created newGraph.graph
function doesNodeExist(node){
    try{
        let len = newGraph.graph.vertices.length;
        for(let i = 0; i<len; i += 1){
            if(newGraph.graph.vertices[i]._id == node._id && newGraph.graph.vertices[i].label == node.label) return true;
            else continue;
        }
        return false;
    }
    catch(error){
        Promise.reject(new Error(`Error at doesNodeExist: ${error}`));
    }
}

//initializes the omitting process. That is, the node with the given nodeID is to be seperated from the whole db.
function omitter(nodeID){
    try{
        let desiredNode = findNode(nodeID);
        newGraph.graph.vertices.push(desiredNode);
        addLinks(desiredNode);
        addNodes(nodeID);
    }
    catch(error){
        Promise.reject(new Error(`Error at omitter: ${error}`));
    }
}

//pushes links where the given node is the target or source
function addLinks(node){
    try{
        let len = links.length;
        for(let i = 0; i<len; i += 1){
            let currLink = links[i];
            if(currLink._outV == node._id || currLink._inV == node._id){
                if( !doesLinkExist(currLink, newGraph.graph.edges) ) newGraph.graph.edges.push(currLink);
            }
            else continue;
        }
    }
    catch(error){
        Promise.reject(new Error(`Error at addLinks: ${error}`));
    }
}

function addNodes(nodeID){
    try{
        let len =  newGraph.graph.edges.length;   
        for(let i = 0; i<len; i += 1){
            let currLink = newGraph.graph.edges[i]; 
            if(currLink._outV == nodeID){
                let nodeToAdd = findNode(currLink._inV);
                if( !(doesNodeExist(nodeToAdd)) ) newGraph.graph.vertices.push(nodeToAdd);
                else continue;
            }
            else if(currLink._inV == nodeID){
                let nodeToAdd = findNode(currLink._outV);
                if( !(doesNodeExist(nodeToAdd)) ) newGraph.graph.vertices.push(nodeToAdd);
                else continue;
            }
            else continue;
        }
    }
    catch(error){
        Promise.reject(new Error(`Error at addNodes: ${error}`));
    }
}

async function mainTrial(){
    //let maxID = findMax(); 
    //let randNodeID = Math.floor(Math.random() * (+maxID - +0)) + +0;;
    randNodeID = 36;
    omitter(randNodeID);    
    var obj = JSON.stringify(newGraph);
    fs.writeFile ("fvvff.json", obj, function(err) {
        if (err) throw err;
        console.log('complete');
        }
    );
}



mainTrial();



/*
function findMax(){
    let len = nodes.length;
    let maxID = nodes[len-1]._id;
    for(let i = 1; i<len; i+= 1){        
        if(maxID < nodes[len-1-i]._id) maxID = nodes[len-1-i]._id;
        else i += 1;
    }
    return maxID;
}*/