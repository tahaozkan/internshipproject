const fs = require('fs');

let rawdata = fs.readFileSync('fvvff.json');
let str = JSON.parse(rawdata);

const graph = {
      nodes: [],
      links: [],
  };

let lenLinks = str.graph.edges.length;
let lenNodes = str.graph.vertices.length;

//initiates the formatting given the parsed data
function formatter(str){
    try{
        formatNodes(str.graph.vertices);
        formatLinks(str.graph.edges);
        nodesListAdder();
        nodesGroupAdder();
    }catch(error){
        return Promise.reject(new Error(`Error at formatter: ${error}`));
    }
}

//nodes is an array of objects which are nodes of the graph; ie, customers
function formatNodes(nodes){
    try{
        for(let i = 0; i<lenNodes; i+=1){
            nodeGenerator(nodes[i]);
        }
    }catch(error){
        return Promise.reject(new Error(`Error at formatNodes: ${error}`));
    }   

}

//generates newNode in the desired format from an existing node, and pushes the node to the nodes array of the graph
function nodeGenerator(node){
    try{
        let newNode = {};
        newNode["id"] = node._id;
        newNode["properties"] = {};
        newNode["type"] = node.label;
        newNode["group"] = {};
        newNode.group["in"] = {};
        newNode.group["out"] = {};
        newNode["targetList"] = [];
        newNode["sourceList"] = [];
    
        let fieldsArrObj = Object.keys(node);       //to acces to the fields of the node using indexes
        let len = fieldsArrObj.length;
        for(let i = 0; i<len; i += 1){
            let field = fieldsArrObj[i];
            if(field == "_id" || field == "_label" || field == "_type") continue; 
            newNode.properties[field] = node[field];
        }
        graph.nodes.push(newNode);
    }catch(error){
        return Promise.reject(new Error(`Error at nodeGenerator: ${error}`));
    }
}

//links is the array of links; ie, links = str.graph.edges
function formatLinks(links){
    try{
        for(let i = 0; i<lenLinks; i+=1){
            linkGenerator(links[i]);
        }
    }catch(error){
        return Promise.reject(new Error(`Error at formatLinks: ${error}`));
    }

}

//generates links and pushes them to the graph dynamically. 
function linkGenerator(link){
    try{    
        if(doesLinkExist(link._outV, link._inV, link._label)){   //if a link with the same source and target IDs exists
            let foundLinkIndex = linkIndex(link._outV, link._inV, link._label);
            let foundLink = graph.links[foundLinkIndex];
            if(foundLink.label == link._label) linkJunctor(link, foundLinkIndex);      //the links are of same type
            else newLinkGenerator(link);                            //same target and source IDs, but different type of transaction           
        }
        else newLinkGenerator(link);                        //a link with the same source and target IDs DNE in the curr graph  
    }catch(error){
        return Promise.reject(new Error(`Error at linkGenerator: ${error}`));
    }
}

//creates new link that DNE 
function newLinkGenerator(link){
    try{
        let source = link._outV; 
        let target = link._inV;
        let newLink = {};
        let newID = source.toString() + "_" + target.toString() + "_" + link._label;
        newLink["id"] = newID;
        newLink["source"] = source;
        newLink["target"] = target;
        newLink["type"] = link._label;
        newLink["label"] = link._label;                                   //should this even exist???????????
        newLink["linkList"] = [link._id];
        newLink["linkCount"] = 1;
        newLink["properties"] = {};   
        
        let fieldsArrObj = Object.keys(link);
        let len = fieldsArrObj.length;
        for(let i = 0; i<len; i += 1){
            let field = fieldsArrObj[i];
            if(field == "_id" || field == "_label" || field == "_type" || field == "_outV" || field == "_inV") continue;
            if(typeof(link[field]) == "number") newLink.properties[field] = link[field];
            else continue;        
        }
        graph.links.push(newLink);
    }catch(error){
        return Promise.reject(new Error(`Error at newLinkGenerator: ${error}`));
    }
}

//returns true if there exits a link with the same source and target IDs AND SAME TRANSACTION TYPE as the given parameters
function doesLinkExist(sourceID, targetID, type){
    try{
        let len = graph.links.length;
        for(let i = 0; i<len; i += 1){
            let currLink = graph.links[i];
            if(currLink == undefined) return false;
            if(currLink.source == sourceID && currLink.target == targetID && currLink.label == type) return true;
        }
        return false;
    }catch(error){
        return Promise.reject(new Error(`Error at doesLinkExist: ${error}`));
    }
}

//returns the index of the link with given source and target IDs AND GIVEN TYPE (MAY BE EFT, HAVALE, ÇEK, SENET)
function linkIndex(sourceID, targetID, type){
    try{
        for(let i = 0; i<lenLinks; i += 1){
            let currLink = graph.links[i];
            if(currLink.source == sourceID && currLink.target == targetID && currLink.label == type) return i;
        }
        return undefined;
    }catch(error){
        return Promise.reject(new Error(`Error at linkIndex: ${error}`));
    }
}

//link, linkToBeJuncted are links of the same type AND have the same source and target IDs, so they need to be merged
function linkJunctor(link, foundLinkIndex){
    try{
        (graph.links[foundLinkIndex]).linkList.push(link._id);
        (graph.links[foundLinkIndex]).linkCount += 1;

        let fieldsArrObj = Object.keys(link);
        let len = fieldsArrObj.length;
        for(let i = 0; i<len; i += 1){
            let currField = fieldsArrObj[i];
            if(typeof(link[currField]) == "number"){           //if the field is a number, ADD
                if( (graph.links[foundLinkIndex]).properties[currField] != undefined ) (graph.links[foundLinkIndex]).properties[currField] += link[currField];
                else continue;
            }
            else continue;                              //if it is a number, it needs to be added          
        }
    }catch(error){
        return Promise.reject(new Error(`Error at linkJunctor: ${error}`));
    }
}

//after creating the links, it sends the nodes to functions to add their targers and sources
function nodesListAdder(){
    try{
        for(let i = 0; i<lenNodes; i+=1){
            nodeTargetListAdder(graph.nodes[i]);
        }
        for(let i = 0; i<lenNodes; i+=1){
            nodeSourceListAdder(graph.nodes[i]);
        }
    }catch(error){
        return Promise.reject(new Error(`Error at nodesListAdder: ${error}`));
    } 
}

//adds the targets of the given node to its target list array as in the form: "<targetID>_<transaction type>"
function nodeTargetListAdder(node){
    try{
        let nodeID = node.id;
        let links = graph.links;
        let len = links.length;
        for(let i = 0; i<len; i += 1){
            if(links[i].source == nodeID){
                let str = (links[i].target).toString() + "_" + (links[i].type).toString();
                if( !(node.targetList).includes(str) ) node.targetList.push(str);                         
            }
            else continue;
        }
        return;
    }catch(error){
        return Promise.reject(new Error(`Error at nodeTargetListAdder: ${error}`));
    }
}

//adds the sources of the given node to its target list array as in the form: "<targetID>_<transaction type>"
function nodeSourceListAdder(node){
    try{
        let nodeID = node.id;
        let links = graph.links;
        let len = links.length;
        for(let i = 0; i<len; i += 1){
            if(links[i].target == nodeID){
                let str = (links[i].source).toString() + "_" + (links[i].type).toString();
                if( !(node.sourceList).includes(str) ) node.sourceList.push(str); 
            }
            else continue;
        }
        return;
    }catch(error){
        return Promise.reject(new Error(`Error at nodeTargetListAdder: ${error}`));
    }
}

//sends all of the nodes to nodeInGroupAdder and nodeOutGroupAdder to add to the group object acordingly
function nodesGroupAdder(){
    try{
        for(let i = 0; i<lenNodes; i+=1){
            nodeInGroupAdder(graph.nodes[i]);
            nodeOutGroupAdder(graph.nodes[i]);
        }
    }catch(error){
        return Promise.reject(new Error(`Error at nodesListAdder: ${error}`));
    } 
}

//adds the in obj to group obj of the node obj :D, which is supposed to be empty till this functiom
function nodeInGroupAdder(node){
    try{
        //check the node's sourcelist, split by _. get the id; and find the link with these id's using linkIndex(sourceID, targetID)
        let sources = node.sourceList;
        let len = sources.length;
        for(let i = 0; i<len; i += 1){
            let splitted = sources[i].split("_");      //splitted = ["<id>", "<transaction type>"]
            let sourceID = Number(splitted[0]);   
            let transactionType = splitted[1];         
            let indexofLink = linkIndex(sourceID, node.id, transactionType);
            let objToAdd = objGenerator(indexofLink); 
            transactionObjAdder(node, objToAdd, transactionType, "in");   
        }
        return;
    }catch(error){
        return Promise.reject(new Error(`Error at nodeTargetListAdder: ${error}`));
    }
}

//adds the out obj to group obj of the node obj :D, which is supposed to be empty till this functiom
function nodeOutGroupAdder(node){
    try{
        //check the node's sourcelist, split by _. get the id; and find the link with these id's using linkIndex(sourceID, targetID)
        let targets = node.targetList;
        let len = targets.length;
        for(let i = 0; i<len; i += 1){
            let splitted = targets[i].split("_");      //splitted = ["<id>", "<transaction type>"]
            let targetID = Number(splitted[0]);   
            let transactionType = splitted[1];         
            let indexofLink = linkIndex(node.id, targetID, transactionType);
            let objToAdd = objGenerator(indexofLink); 
            transactionObjAdder(node, objToAdd, transactionType, "out");   
        }
        return;
    }catch(error){
        return Promise.reject(new Error(`Error at nodeTargetListAdder: ${error}`));
    }
}

//creates the obj to be added
function objGenerator(index){
    try{
        let newObj = {};
        let link = graph.links[index]
        let fieldsArrObj = Object.keys(link.properties);
        let len = fieldsArrObj.length;    
        for(let i = 0; i<len; i += 1){
            let field = fieldsArrObj[i];
            newObj[field] = link.properties[field];        
        }
        return newObj;
    }catch(error){
        Promise.reject(new Error(`Error at objGenerator: ${error}`));
    }
}

//adds the fields of transactions of the created obj with those of prev ones' of the node
function transactionObjAdder(node, objToAdd, transactionType, type){
    try{
        let objToCheck = node.group[type];
        if(objToCheck[transactionType] == undefined){
            objToCheck[transactionType] = {};
            let fieldsArrObj = Object.keys(objToAdd);
            let len = fieldsArrObj.length;
            for(let i = 0; i<len; i += 1){
                let field = fieldsArrObj[i];
                let nodeWithType = node.group[type];
                let nodeTransactionWithType = nodeWithType[transactionType]; 
                nodeTransactionWithType[field] = objToAdd[field];
            }
        }
        else{
            let fieldsArrObj = Object.keys(objToAdd);
            let len = fieldsArrObj.length;
            for(let i = 0; i<len; i += 1){
                let field = fieldsArrObj[i];
                let nodeWithType = node.group[type];
                let nodeTransactionWithType = nodeWithType[transactionType];
                nodeTransactionWithType[field] += objToAdd[field];
            }
        }
    }catch(error){
        Promise.reject(new Error(`Error at transactionObjAdder: ${error}`));
    }

}

formatter(str);

async function mainTrial(){    
    var obj = JSON.stringify(graph);
    fs.writeFile ("formattedDataONDOUBLE.json", obj, function(err) {
        if (err) throw err;
        console.log('complete');
        }
    );

}

mainTrial();